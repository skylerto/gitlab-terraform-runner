FROM registry.gitlab.com/tmaczukin-test-projects/fargate-driver-debian:latest

RUN apt-get install zip -y

ENV TERRAFORM_VERSION 0.14.7

RUN curl -Lo /terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip /terraform.zip && mv /terraform /usr/local/bin/terraform
